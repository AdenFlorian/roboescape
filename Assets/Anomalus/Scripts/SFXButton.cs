﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class SFXButton : MonoBehaviour {

	public Texture speakerOn;
	public Texture speakerOff;

	public bool musicOn = true;
	/*
	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}
	*/
	void OnMouseUpAsButton() {

		GameManager.Instance.SwitchSFXOnOff();

		if (musicOn) {

			renderer.material.mainTexture = speakerOff;

		} else {

			renderer.material.mainTexture = speakerOn;

		}

		musicOn = !musicOn;

	}
	/*
	void OnMouseOver() {



	}

	void OnMouseEnter() {



	}

	void OnMouseExit() {



	}
	 */
}
