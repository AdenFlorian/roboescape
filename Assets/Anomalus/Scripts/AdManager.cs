﻿using UnityEngine;
using System.Collections;

public class AdManager : MonoBehaviour {

	void Start() {

		Debug.Log("AdManagerSTART");

		Init();

	}

	public void Init() {

#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		Debug.Log("Started");
        AdMobPlugin.CreateBannerView("ca-app-pub-4488807059001653/6110080357",
                                     AdMobPlugin.AdSize.Banner,
                                     false);
        Debug.Log("Created Banner View");
        AdMobPlugin.RequestBannerAd(true);
        Debug.Log("Requested Banner Ad");
#endif

	}

	void OnEnable() {
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		Debug.Log("Registering for AdMob Events");
        AdMobPlugin.ReceivedAd += HandleReceivedAd;
        AdMobPlugin.FailedToReceiveAd += HandleFailedToReceiveAd;
        AdMobPlugin.ShowingOverlay += HandleShowingOverlay;
        AdMobPlugin.DismissedOverlay += HandleDismissedOverlay;
        AdMobPlugin.LeavingApplication += HandleLeavingApplication;
#endif
	}

	void OnDisable() {
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
        Debug.Log("Unregistering for AdMob Events");
		AdMobPlugin.ReceivedAd -= HandleReceivedAd;
        AdMobPlugin.FailedToReceiveAd -= HandleFailedToReceiveAd;
        AdMobPlugin.ShowingOverlay -= HandleShowingOverlay;
        AdMobPlugin.DismissedOverlay -= HandleDismissedOverlay;
        AdMobPlugin.LeavingApplication -= HandleLeavingApplication;
#endif
	}

	public void HandleReceivedAd() {
		Debug.Log("HandleReceivedAd event received");
	}

	public void HandleFailedToReceiveAd(string message) {
		Debug.Log("HandleFailedToReceiveAd event received with message:");
		Debug.Log(message);
	}

	public void HandleShowingOverlay() {
		Debug.Log("HandleShowingOverlay event received");
	}

	public void HandleDismissedOverlay() {
		Debug.Log("HandleDismissedOverlay event received");
	}

	public void HandleLeavingApplication() {
		Debug.Log("HandleLeavingApplication event received");
	}
}
