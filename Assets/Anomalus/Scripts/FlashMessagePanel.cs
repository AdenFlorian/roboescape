﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlashMessagePanel : MonoBehaviour {

	public static FlashMessagePanel Instance;

	public bool showMessages = true;

	public float messageFadeOutStart = 3;
	public float messageFadeOutTime = 1;

	List<FlashMessageContainer> messageQueue = new List<FlashMessageContainer>();

	public Color messageColor = Color.white;

	public GUISkin guiSkin;

	public bool demoMode = false;

	public bool showDebugLog = false;
	public bool showStackTrace = false;

	public enum FMPPanelPosition {
		Center,
		Left,
		TopLeft,
		Top,
		TopRight,
		Right,
		BottomRight,
		Bottom,
		BottomLeft
	}
	public FMPPanelPosition panelPosition = FMPPanelPosition.Bottom;
	/*
	public enum FMPMessageAlignment {
		Left,
		Center,
		Right
	}
	public FMPMessageAlignment messageAlignment = FMPMessageAlignment.Center;
	*/
	public enum FMPFlowDirection {
		Up,
		Down
	}
	public FMPFlowDirection flowDirection = FMPFlowDirection.Up;

	// Use this for initialization
	void Start () {

		Instance = this;

		if (demoMode) {
			StartCoroutine(AddMessageLoop());
		}

		

		Application.RegisterLogCallback(HandleLog);

	}

	void HandleLog(string logString, string stackTrace, LogType type) {

		if (showDebugLog) {
			FlashMessagePanel.Instance.Add(logString + (showStackTrace ? "\n" + stackTrace : ""));
		}
		
	}
	
	// Update is called once per frame
	void Update () {

		guiSkin.label.fontSize = Screen.height / 37;
	
	}

	IEnumerator AddMessageLoop() {

		while (true) {

			Add(Time.deltaTime.ToString());

			yield return new WaitForSeconds(1);

		}

	}

	public void RemoveMessage(FlashMessageContainer objectToRemove) {

		messageQueue.Remove(objectToRemove);

	}

	void OnGUI() {

		if (showMessages) {

			GUI.skin = guiSkin;

			GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));

			switch (panelPosition) {
				case FMPPanelPosition.Center:
					GUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					GUILayout.BeginVertical();
					GUILayout.FlexibleSpace();
					break;
				case FMPPanelPosition.Left:
					GUILayout.BeginHorizontal();
					GUILayout.BeginVertical();
					GUILayout.FlexibleSpace();
					break;
				case FMPPanelPosition.TopLeft:
					GUILayout.BeginHorizontal();
					GUILayout.BeginVertical();
					break;
				case FMPPanelPosition.Top:
					GUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					GUILayout.BeginVertical();
					break;
				case FMPPanelPosition.TopRight:
					GUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					GUILayout.BeginVertical();
					break;
				case FMPPanelPosition.Right:
					GUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					GUILayout.BeginVertical();
					GUILayout.FlexibleSpace();
					break;
				case FMPPanelPosition.BottomRight:
					GUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					GUILayout.BeginVertical();
					GUILayout.FlexibleSpace();
					break;
				case FMPPanelPosition.Bottom:
					GUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					GUILayout.BeginVertical();
					GUILayout.FlexibleSpace();
					break;
				case FMPPanelPosition.BottomLeft:
					GUILayout.BeginHorizontal();
					GUILayout.BeginVertical();
					GUILayout.FlexibleSpace();
					break;
				default:
					break;
			}

			if (messageQueue.Count > 0) {

				switch (flowDirection) {
					case FMPFlowDirection.Up:
						/*
						foreach (FlashMessageContainer messageContainer in messageQueue) {
							GUI.color = messageContainer.messageColor;
							GUILayout.Label(messageContainer.messageString);
						}
						*/
						for (int i = 0; i < messageQueue.Count; i++) {
							GUILayout.BeginHorizontal();
							switch (panelPosition) {
								case FMPPanelPosition.Center:
								case FMPPanelPosition.Top:
								case FMPPanelPosition.Bottom:
								case FMPPanelPosition.TopRight:
								case FMPPanelPosition.Right:
								case FMPPanelPosition.BottomRight:
									GUILayout.FlexibleSpace();
									break;
								case FMPPanelPosition.Left:
								case FMPPanelPosition.TopLeft:
								case FMPPanelPosition.BottomLeft:
								default:
									break;
							}
							GUI.color = messageQueue[i].messageColor;
							GUILayout.Label(messageQueue[i].messageString);
							switch (panelPosition) {
								case FMPPanelPosition.Center:
								case FMPPanelPosition.Top:
								case FMPPanelPosition.Bottom:
								case FMPPanelPosition.Left:
								case FMPPanelPosition.TopLeft:
								case FMPPanelPosition.BottomLeft:
									GUILayout.FlexibleSpace();
									break;
								case FMPPanelPosition.TopRight:
								case FMPPanelPosition.Right:
								case FMPPanelPosition.BottomRight:
								default:
									break;
							}
							GUILayout.EndHorizontal();
						}
						break;
					case FMPFlowDirection.Down:
						for (int i = (messageQueue.Count - 1); i >= 0; i--) {
							GUILayout.BeginHorizontal();
							switch (panelPosition) {
								case FMPPanelPosition.Center:
								case FMPPanelPosition.Top:
								case FMPPanelPosition.Bottom:
								case FMPPanelPosition.TopRight:
								case FMPPanelPosition.Right:
								case FMPPanelPosition.BottomRight:
									GUILayout.FlexibleSpace();
									break;
								case FMPPanelPosition.Left:
								case FMPPanelPosition.TopLeft:
								case FMPPanelPosition.BottomLeft:
								default:
									break;
							}
							GUI.color = messageQueue[i].messageColor;
							GUILayout.Label(messageQueue[i].messageString);
							switch (panelPosition) {
								case FMPPanelPosition.Center:
								case FMPPanelPosition.Top:
								case FMPPanelPosition.Bottom:
								case FMPPanelPosition.Left:
								case FMPPanelPosition.TopLeft:
								case FMPPanelPosition.BottomLeft:
									GUILayout.FlexibleSpace();
									break;
								case FMPPanelPosition.TopRight:
								case FMPPanelPosition.Right:
								case FMPPanelPosition.BottomRight:
								default:
									break;
							}
							GUILayout.EndHorizontal();
						}
						break;
					default:
						break;
				}

			}


			switch (panelPosition) {
				case FMPPanelPosition.Center:
					GUILayout.FlexibleSpace();
					GUILayout.EndVertical();
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					break;
				case FMPPanelPosition.Left:
					GUILayout.FlexibleSpace();
					GUILayout.EndVertical();
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					break;
				case FMPPanelPosition.TopLeft:
					GUILayout.FlexibleSpace();
					GUILayout.EndVertical();
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					break;
				case FMPPanelPosition.Top:
					GUILayout.FlexibleSpace();
					GUILayout.EndVertical();
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					break;
				case FMPPanelPosition.TopRight:
					GUILayout.FlexibleSpace();
					GUILayout.EndVertical();
					GUILayout.EndHorizontal();
					break;
				case FMPPanelPosition.Right:
					GUILayout.FlexibleSpace();
					GUILayout.EndVertical();
					GUILayout.EndHorizontal();
					break;
				case FMPPanelPosition.BottomRight:
					GUILayout.EndVertical();
					GUILayout.EndHorizontal();
					break;
				case FMPPanelPosition.Bottom:
					GUILayout.EndVertical();
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					break;
				case FMPPanelPosition.BottomLeft:
					GUILayout.EndVertical();
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
					break;
				default:
					break;
			}

			GUILayout.EndArea();
			
		}

		

	}

	public void Add(string message) {

		Add(message, messageColor);

	}

	public void Add(string message, Color newColor) {

		//Debug.Log("ADD");

		FlashMessageContainer newMessCont = gameObject.AddComponent<FlashMessageContainer>();

		newMessCont.messageString = message;
		newMessCont.messageColor = newColor;

		messageQueue.Add(newMessCont);

	}

	

}
