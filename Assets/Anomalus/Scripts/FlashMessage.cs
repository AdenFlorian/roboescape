﻿using UnityEngine;
using System.Collections;

public class FlashMessageContainer : MonoBehaviour {

	public Color messageColor = Color.red;

	public string messageString = "ERROR";

	void Start() {

		StartCoroutine(FadeOutAndDelete());

	}

	IEnumerator FadeOutAndDelete() {

		yield return new WaitForSeconds(FlashMessagePanel.Instance.messageFadeOutStart);

		while (messageColor.a > 0) {

			messageColor.a -= 0.01f;

			yield return new WaitForSeconds(FlashMessagePanel.Instance.messageFadeOutTime / 100);

		}

		FlashMessagePanel.Instance.RemoveMessage(this);

		Destroy(this);

	}

}
