﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public static MenuManager Instance;

	public GUISkin menuGUISkin;
	public GUISkin debugGUISkin;

	int buttonWidth = (int)((float)Screen.width * 0.85f);

	public enum CurrentMenu {
		Main,
		Store,
		Options,
		DevOptions,
		Credits,
		Loading,
		EndOfRound,
		None
	}
	CurrentMenu currentMenu = CurrentMenu.Loading;

	public bool showDebugInfo = false;
	public bool showDebugMenu = false;
	public bool showStoreMenu = false;
	public bool showOptionsMenu = false;

	// Use this for initialization
	void Start () {

		Instance = this;

		// Initialize properties here


		

	}
	
	// Update is called once per frame
	void Update () {

		buttonWidth = (int)((float)Screen.width * 0.85f);

		menuGUISkin.button.fontSize = Screen.height / 10;
		debugGUISkin.label.fontSize = Screen.height / 35;
	
	}

	// OnGUI
	void OnGUI() {

		GUI.skin = debugGUISkin;

		if (showDebugInfo) {
			DrawDebugInfo();
		}

		GUI.skin = menuGUISkin;

		GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));

		GUILayout.BeginHorizontal();

		GUILayout.FlexibleSpace();

		// Decides which menu to show
		switch (currentMenu) {
			case CurrentMenu.Main:
				DrawMainMenu();
				break;
			case CurrentMenu.Store:
				DrawStoreMenu();
				break;
			case CurrentMenu.Options:
				DrawOptionsMenu();
				break;
			case CurrentMenu.DevOptions:
				DrawDevOptionsMenu();
				break;
			case CurrentMenu.Credits:
				DrawCreditsMenu();
				break;
			case CurrentMenu.Loading:
				DrawLoadingMenu();
				break;
			case CurrentMenu.EndOfRound:
				DrawRoundEndScreen();
				break;
			case CurrentMenu.None:
				break;
			default:
				Debug.Log(@"Error in MenuManager.OnGUI()");
				break;
		}

		GUILayout.FlexibleSpace();

		GUILayout.EndHorizontal();

		GUILayout.EndArea();

	}

	// GUI Draw and Show functions
	#region GUIDrawAndShowFunctions

	public void HideAllMenus() {

		currentMenu = CurrentMenu.None;

	}

	public void ShowMainMenu() {

		currentMenu = CurrentMenu.Main;

	}

	void DrawMainMenu() {

		GUILayout.BeginVertical();

		GUILayout.FlexibleSpace();

		if (GUILayout.Button("START!", GUILayout.Width(buttonWidth))) {

			GameManager.Instance.StartRound();

		}

#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR

		if (!Social.localUser.authenticated) {

			if (GUILayout.Button("SIGN IN", GUILayout.Width(buttonWidth))) {
				Debug.Log("Signin called");
				SocialManager.SignIn();
			}

		} else {

			if (GUILayout.Button("SIGN OUT", GUILayout.Width(buttonWidth))) {
				Debug.Log("Signout called");
				SocialManager.SignOut();
			}

		}

#endif

		if (showStoreMenu && GUILayout.Button("STORE", GUILayout.Width(buttonWidth))) {

			currentMenu = CurrentMenu.Store;

		}

		if (showOptionsMenu && GUILayout.Button("OPTIONS", GUILayout.Width(buttonWidth))) {

			currentMenu = CurrentMenu.Options;

		}

		if (showDebugMenu && GUILayout.Button("DEV OPTIONS", GUILayout.Width(buttonWidth))) {

			currentMenu = CurrentMenu.DevOptions;

		}

		if (GUILayout.Button("CREDITS", GUILayout.Width(buttonWidth))) {

			currentMenu = CurrentMenu.Credits;

		}

		// Only show on paltforms that let you exit
#if UNITY_STANDALONE && !UNITY_EDITOR

		if (GUILayout.Button("QUIT")) {

			Application.Quit();

		}

#endif

		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();

		

	}

	public void ShowStoreMenu() {

		currentMenu = CurrentMenu.Main;

	}

	void DrawStoreMenu() {

		GUILayout.BeginVertical();

		GUILayout.FlexibleSpace();

		if (GUILayout.Button("BACK", GUILayout.Width(buttonWidth))) {

			currentMenu = CurrentMenu.Main;

		}

		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();

	}

	public void ShowOptionsMenu() {

		currentMenu = CurrentMenu.Options;

	}

	void DrawOptionsMenu() {

		GUILayout.BeginVertical();

		GUILayout.FlexibleSpace();

		if (GUILayout.Button("BACK", GUILayout.Width(buttonWidth))) {

			currentMenu = CurrentMenu.Main;

		}

		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();

	}

	public void ShowDevOptionsMenu() {

		currentMenu = CurrentMenu.DevOptions;

	}

	void DrawDevOptionsMenu() {



		GUILayout.BeginVertical();

		GUILayout.FlexibleSpace();

		if (GUILayout.Button("RESET PREFS", GUILayout.Width(buttonWidth))) {

			PrefsManager.Instance.ResetPrefs();

		}

		if (GUILayout.Button("BACK", GUILayout.Width(buttonWidth))) {

			currentMenu = CurrentMenu.Main;

		}

		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();

	}

	public void ShowCreditsMenu() {

		currentMenu = CurrentMenu.Credits;

	}

	void DrawCreditsMenu() {

		GUILayout.BeginVertical();

		GUILayout.FlexibleSpace();

		GUI.color = Color.red;

		GUILayout.Label("Anomalus Software");

		GUILayout.Label("David \"AdenFlorian\" Valachovic");

		GUI.color = Color.white;

		if (GUILayout.Button("BACK", GUILayout.Width(buttonWidth))) {

			currentMenu = CurrentMenu.Main;

		}

		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();

	}

	public void ShowLoadingMenu() {

		currentMenu = CurrentMenu.Loading;

	}

	void DrawLoadingMenu() {

		GUILayout.BeginVertical();

		GUILayout.FlexibleSpace();

		GUILayout.Label("LOADING...", GUILayout.Width(buttonWidth));

		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();

	}

	public void ShowRoundEndScreen() {

		currentMenu = CurrentMenu.EndOfRound;

	}

	void DrawRoundEndScreen() {

		GUILayout.BeginVertical();

		GUILayout.FlexibleSpace();
		
#if (UNITY_ANDROID || UNITY_IPHONE) //&& !UNITY_EDITOR

		// If new high score, 
		//if (GameManager.Instance.newLongestDistance) {

			// And user is signed in
			if (Social.localUser.authenticated) {
				/*
				// Show submit score button
				if (GUILayout.Button("SUBMIT DISTANCE", GUILayout.Width(buttonWidth))) {

					SocialManager.SubmitNewLongestDistance(GameManager.Instance.distanceHighScore);

					GameManager.Instance.newLongestDistance = false;

				}
				*/

				// Show leaderboards button
				if (GUILayout.Button("LEADERBOARDS", GUILayout.Width(buttonWidth))) {
					SocialManager.ShowLongestDistanceLeaderboard();
				}

			} else {	// Else if not signed in

				// Show signin button
				if (GUILayout.Button("SIGN IN", GUILayout.Width(buttonWidth))) {
					Debug.Log("Signin called");
					SocialManager.SignIn();
				}

			}
			/*
		} else {	// If not new longest distance, or distance alreadysubmitted

			// Show signin button
			if (GUILayout.Button("LEADERBOARD", GUILayout.Width(buttonWidth))) {

				SocialManager.ShowLongestDistanceLeaderboard();

			}

		}*/

#endif
		
		if (GUILayout.Button("PLAY AGAIN", GUILayout.Width(buttonWidth))) {

			GameManager.Instance.StartRound();

		}

		if (GUILayout.Button("MAIN MENU", GUILayout.Width(buttonWidth))) {

			ShowMainMenu();

		}

		GUILayout.FlexibleSpace();

		GUILayout.EndVertical();

	}

	void DrawDebugInfo() {

		GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));

		GUILayout.BeginHorizontal();

		GUILayout.FlexibleSpace();

		GUILayout.BeginVertical();

		//GUILayout.Label(FPSDisplay.Instance.guiText.text);
		/*
		if (GameManager.Instance.currentPlayerObject != null) {
			GUILayout.Label("Y VELOCITY: " + GameManager.Instance.currentPlayerObject.rigidbody2D.velocity.y.ToString());
			GUILayout.Label("GROUNDED: " + GameManager.Instance.currentPlayerMonoBehavior.isGrounded);
		} else {
			GUILayout.Label("Y VELOCITY: ");
			GUILayout.Label("GROUNDED: ");
		}
		*/
		
		GUILayout.Label("HIGHSCORE DISTANCE: " + GameManager.Instance.distanceHighScore.ToString());

		if (GameManager.Instance.currentPlayerObject != null) {
			GUILayout.Label("CURRENT DISTANCE: " + GameManager.Instance.distanceTraveledReal.ToString());
		}

		//GUILayout.Label("COINS: " + GameManager.Instance.coinsTotal.ToString());
		
		GUILayout.EndVertical();

		GUILayout.EndHorizontal();

		GUILayout.EndArea();

	}

	#endregion 


}

