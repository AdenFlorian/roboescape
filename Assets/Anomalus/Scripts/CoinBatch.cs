﻿using UnityEngine;
using System.Collections;

public class CoinBatch : MonoBehaviour {

	public bool canBeRotatedOnSpawn = false;
	public bool canRotateWhileAlive = false;
	public bool canSpawnInDoubles = false;

	public float getCoinVolume = 0.5f;

	int coinsInBatch;

	bool bonusGiven = false;

	// Use this for initialization
	void Start() {

		coinsInBatch = transform.childCount;

	}

	// Update is called once per frame
	void Update() {

		if (!bonusGiven) {

			// If player got all coins, give bonus
			if (coinsInBatch == 0) {

				// Give bonus coins
				GameManager.Instance.coinsTotal += 5;

				// Play bonus sound
				SoundManager.PlaySFX(SoundManager.Load("GotAllCoinsInBatch01"), getCoinVolume);

				Debug.Log("Got All Coins In Batch!! +5 Coins!!");

				bonusGiven = true;

			}
			
		}
		

		

		if (transform.position.x < -5) {

			Destroy(this.gameObject);

		}

		Move();

		if (canRotateWhileAlive) {
			Rotate();
		}

	}

	public void RemoveOneCoin() {

		coinsInBatch--;

	}

	void Move() {

		transform.position += new Vector3(-GameManager.Instance.scrollSpeed * Time.deltaTime, 0, 0);

	}

	void Rotate() {

		transform.Rotate(0, 0, 180f * Time.deltaTime);

	}
}
