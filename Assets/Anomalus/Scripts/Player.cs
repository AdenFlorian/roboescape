﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float jumpForce = 3f;
	public float jetpackForce = 700f;
	public Transform groundCheck;
	//int jumpCount = 0;
	public Vector2 rigid2DVelocity;
	public AudioSource runningSound;

	Animator animator;

	public float runningSoundVolume = 0.8f;
	public float landSoundVolume = 0.2f;

	// Collider settings
	bool collideWithObstacles = true;

	public bool mutedSFX = false;


	// Movement settings
	bool acceptPlayerInput = true;
	public bool isGrounded { get; private set; }
	bool wasGroundedLastFrame = false;
	//bool jumping = false;
	public JumpMode jumpMode = JumpMode.Jetpack;
	//float lastY;
	//bool isJetpackOn = true;
	//bool jump = false;

	// Use this for initialization
	void Start () {

		// Initialize fields/props
		collideWithObstacles = true;
		acceptPlayerInput = true;
		isGrounded = false;

		if (animator == null) {
			animator = GetComponent<Animator>();
		}
		
	}

	void OnEnable() {

		Start();

	}
	
	// Update is called once per frame
	void Update () {

		if (acceptPlayerInput) {
			HandleInput();
		}
		
	
	}

	void FixedUpdate() {
		/*
		if (jumping) {
			if (rigidbody2D.velocity.y <= 0) {
				jumping = false;
			}
		}

		if (jump) {
			rigidbody2D.AddForce(new Vector2(0, jumpForce));
			jump = false;
			isGrounded = false;
			jumping = true;
		}
		*/

		
		wasGroundedLastFrame = isGrounded;

		isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

		if (acceptPlayerInput) {

			if (!wasGroundedLastFrame && isGrounded) {
				SoundManager.PlaySFX(SoundManager.Load("Land01"), landSoundVolume);
			}

			if (isGrounded) {
				animator.SetBool("Grounded", true);
				runningSound.volume = runningSoundVolume;
			} else {
				animator.SetBool("Grounded", false);
				runningSound.volume = 0;
			}



			if (jumpMode == JumpMode.Jetpack && (Input.GetKey(KeyCode.Space) || Input.touchCount > 0)) {
				rigidbody2D.AddForce(new Vector2(0, jetpackForce * Time.deltaTime));
				TurnJetpackOn();
				//Debug.Log("TEST" + Time.timeSinceLevelLoad);
			} else {
				TurnJetpackOff();
			}
			
		}

		if (mutedSFX) {
			runningSound.volume = 0;
		}

	}

	void TurnJetpackOn() {

		animator.SetBool("JetpackOn", true);
		// Turn jetpack volume to 1
		audio.volume = .2f;

		if (mutedSFX) {
			audio.volume = 0;
		}

	}

	void TurnJetpackOff() {

		animator.SetBool("JetpackOn", false);
		audio.volume = 0;

	}

	void HandleInput() {

		/*
		if (!jumping) {
			isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		}
		*/

		/*
		if (Input.GetKeyDown(KeyCode.Space) || Input.touchCount > 0) {
			
			switch (jumpMode) {
				case JumpMode.Jump:
					if (isGrounded) {
						jump = true;
					}
					break;
				case JumpMode.GravitySwitch:
					Physics2D.gravity = -Physics2D.gravity;
					break;
				case JumpMode.Jetpack:
					
					break;
				default:
					break;
			}
			
		}
		*/

	}

	/*void OnCollisionEnter2D(Collision2D colInfo) {

		string tagCollidedWith = colInfo.gameObject.tag;

		//Debug.Log(colInfo.gameObject.tag);

		switch (tagCollidedWith) {
			case "obstacle":
				Debug.Log("HIT");
				GameManager.Instance.EndRound();
				break;
			case "coin":
				Debug.Log("Got coin!");
				GameManager.Instance.coinsGotThisRound++;
				// Play coin get animation on coin and/or player
				colInfo.gameObject.GetComponent<Animator>().SetBool("GotCoin", true);
				// Turn coin collider off
				colInfo.gameObject.collider2D.enabled = false;
				// Destroy coin when animation is complete
				StartCoroutine(DestroyAfterNSeconds(colInfo.gameObject, 2));
				//Destroy(colInfo.gameObject);
				break;
			default:
				break;
		}

	}*/

	void OnTriggerEnter2D(Collider2D col) {

		string tagCollidedWith = col.gameObject.tag;

		switch (tagCollidedWith) {
			case "obstacle":
				if (collideWithObstacles) {
					OnCollideWithObstacle(col);
				}
				break;
			case "coin":
				OnCollideWithCoin(col);
				break;
			default:
				break;
		}

	}

	void OnCollideWithObstacle(Collider2D col) {

		Debug.Log("HIT OBSTACLE");

		// Disable collision with obstacles
		collideWithObstacles = false;

		// Turn off user input
		acceptPlayerInput = false;

		TurnJetpackOff();

		// Let character be able to spin
		rigidbody2D.fixedAngle = false;

		// Spin character
		rigidbody2D.AddTorque(-3f);

		StartCoroutine(EndRoundInNSeconds(3));

		GameManager.Instance.OnPlayerDeath();

	}

	IEnumerator EndRoundInNSeconds(int n) {

		yield return new WaitForSeconds(n);

		GameManager.Instance.EndRound();

	}

	void OnCollideWithCoin(Collider2D col) {

		//Debug.Log("Got coin!");
		// Increase coins
		GameManager.Instance.coinsTotal++;
		// Tell the associated coin batch that player got a coin
		col.transform.parent.GetComponent<CoinBatch>().RemoveOneCoin();
		// Play coin get animation on coin and/or player
		col.gameObject.GetComponent<Animator>().SetBool("GotCoin", true);
		// Play getcoin sound from coin
		col.gameObject.audio.Play();
		// Disable colliders on coin
		col.gameObject.collider2D.enabled = false;
		// Destroy coin when animation is complete
		StartCoroutine(DestroyAfterNSeconds(col.gameObject, 2));

	}

	IEnumerator DestroyAfterNSeconds(GameObject destroyMe, int n) {

		yield return new WaitForSeconds(n);


		Destroy(destroyMe);

	}

	public enum JumpMode {
		Jump,
		GravitySwitch,
		Jetpack
	}
}
