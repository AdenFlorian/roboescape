﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	public bool canBeRotatedOnSpawn = false;
	public bool canRotateWhileAlive = false;
	public bool canSpawnInDoubles = false;
	bool rotateReverse = false;

	// Use this for initialization
	void Start () {

		collider2D.isTrigger = true;

		rotateReverse = Random.Range(0, 100) > 50 ? true : false;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position.x < -5) {

			Destroy(this.gameObject);

		}

		Move();

		if (canRotateWhileAlive) {
			Rotate();
		}
	
	}

	void Move() {

		transform.position += new Vector3(-GameManager.Instance.scrollSpeed * Time.deltaTime, 0, 0);

	}

	void Rotate() {

		transform.Rotate(0, 0, (Random.Range(10f, 15f) * (rotateReverse ? -1 : 1)) * Time.deltaTime * GameManager.Instance.difficulty);

	}
}
