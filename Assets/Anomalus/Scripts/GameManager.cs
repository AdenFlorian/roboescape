﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;

	public GameObject topAndBottomColliders;
	public GameObject playerPrefab;
	Vector3 playerSpawnPosition;

	public GameObject currentPlayerObject { get; private set; }
	public Player currentPlayerMonoBehavior { get; private set; }

	// Difficulty fields
	public int difficulty { get; private set; }
	public int difficultyIncreaseRate = 10;	// Increase difficulty after n seconds
	public int startingDifficulty = 1;

	// Obstacle scrolling fields
	public float scrollSpeed { get; private set; }	// How fast things should scroll
	public float startScrollSpeed = 1;	// The starting scroll speed
	public float scrollSpeedStep = 0.1f;	// How much to increase the scrollSpeed by
	public int scrollSpeedIncreaseRate = 1;	// Increase scroll speed every n seconds

	// Distance fields
	float distanceTraveledFloat = 0;
	public int distanceTraveledReal = 0;
	public int distanceHighScore = 0;
	public bool newLongestDistance { get; set; }
	public int totalDistanceForUser = 0;	// Get from Social

	// Round fields
	public bool roundStarted { get; private set; }

	// Coin fields
	public int coinsTotal = 0;

	// Audio
	float audioStart;

	// Use this for initialization
	void Start () {

		Debug.Log("GameManager START");

		Instance = this;

		// Initialize properties here
		difficulty = startingDifficulty;
		scrollSpeed = startScrollSpeed;
		playerSpawnPosition = new Vector3(-.85f * (Screen.width / Screen.height), 0, 0);
		roundStarted = false;

		// Once done loading, show main menu
		MenuManager.Instance.ShowMainMenu();

		audioStart = audio.volume;

	}
	
	// Update is called once per frame
	void Update () {

		if (roundStarted) {
			IncreaseDistance();
		}

	}

	void IncreaseDistance() {

		distanceTraveledFloat += scrollSpeed * Time.deltaTime;

		distanceTraveledReal = (int)distanceTraveledFloat;

	}

	IEnumerator IncreaseScrollSpeed(int n) {

		while (true) {

			yield return new WaitForSeconds(n);

			scrollSpeed += scrollSpeedStep;

		}
		
	}

	IEnumerator IncreaseDifficulty(int n) {

		while (true) {

			yield return new WaitForSeconds(n);

			difficulty++;

		}

	}

	IEnumerator SlowScrollSpeedToStop() {

		do {

			yield return new WaitForEndOfFrame();

			scrollSpeed -= scrollSpeedStep * Time.deltaTime * 50;

		} while (scrollSpeed >= 0);

		scrollSpeed = 0;

	}

	public void StartRound() {

		Debug.Log("Round started!");

		// Reset fields that need to be reset
		difficulty = startingDifficulty;
		scrollSpeed = startScrollSpeed;
		distanceTraveledFloat = 0;
		distanceTraveledReal = 0;
		newLongestDistance = false;
		SocialManager.SetDistancesSubmittedFalse();

		// Hide menu
		MenuManager.Instance.HideAllMenus();

		// Hide ads
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		AdMobPlugin.HideBannerView();
#endif

		// Spawn player
		if (currentPlayerObject == null) {
			currentPlayerObject = Instantiate(playerPrefab, playerSpawnPosition, Quaternion.identity) as GameObject;
			currentPlayerMonoBehavior = currentPlayerObject.GetComponent<Player>();
		} else {
			currentPlayerObject.transform.position = playerSpawnPosition;
			currentPlayerObject.SetActive(true);
		}
		

		// Enable ground and ceiling
		topAndBottomColliders.SetActive(true);

		// Push player forward
		//newPlayer.rigidbody2D.AddForce(new Vector2(1000, 0));

		// Start coroutines for scroll speed, difficulty
		StartCoroutine(IncreaseScrollSpeed(scrollSpeedIncreaseRate));
		StartCoroutine(IncreaseDifficulty(difficultyIncreaseRate));

		// Start obstacles spawning
		ObstacleSpawner.Instance.StartSpawning();



		roundStarted = true;

	}

	public void OnPlayerDeath() {

		StopAllCoroutines();

		StartCoroutine(SlowScrollSpeedToStop());



	}

	public void EndRound() {

		roundStarted = false;
		
		// Unload game stuff
		ObstacleSpawner.Instance.StopSpawning();
		ObstacleSpawner.Instance.RemoveAllObstacles();

		// Stop scroll speed increase coroutine
		StopAllCoroutines();

		// Disable player object
		//currentPlayerObject.SetActive(false);

		// Destroy player object
		Destroy(currentPlayerObject);

		// Log high score
		if (distanceTraveledReal > distanceHighScore) {
			distanceHighScore = distanceTraveledReal;
			PrefsManager.Instance.SetNewLongestDistance(distanceHighScore);
			newLongestDistance = true;	// Must reset on round start
			FlashMessagePanel.Instance.Add("New Longest Distance!");
		}

		// Save coins to prefs
		PrefsManager.Instance.SetCoinTotal(coinsTotal);

		// Save prefs
		PlayerPrefs.Save();

#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		// Submit distance to Social if on mobile
		if (Social.localUser.authenticated) {
			SocialManager.SubmitDistances();
	   }
#endif

		Debug.Log("Round Ended!");

		Debug.Log("Back to main menu...");

		// Show interstitial ad

		// Show banner ad
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		AdMobPlugin.ShowBannerView();
#endif

		// Show round end screen
		MenuManager.Instance.ShowRoundEndScreen();

	}

	public void SwitchMusicOnOff() {

		if (audio.volume == 0) {
			audio.volume = audioStart;
		} else {
			audio.volume = 0;
		}

	}

	public void SwitchSFXOnOff() {

		currentPlayerMonoBehavior.mutedSFX = SoundManager.MuteSFX();

	}

}
