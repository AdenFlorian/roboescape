﻿using UnityEngine;
using System.Collections;

public class PrefsManager : MonoBehaviour {

	public static PrefsManager Instance;

	// Keys
	string longestDistanceKey = "longestDistance";
	string coinAmountKey = "coinAmount";

	// Use this for initialization
	void Start() {

		Instance = this;

		// Initialize properties here
		int longDistResult = PlayerPrefs.GetInt(longestDistanceKey, -1);

		if (longDistResult >= 0) {

			GameManager.Instance.distanceHighScore = longDistResult;

		} else {

			PlayerPrefs.SetInt(longestDistanceKey, 0);
			GameManager.Instance.distanceHighScore = 0;

		}

		int coinAmountResult = PlayerPrefs.GetInt(coinAmountKey, -1);

		if (coinAmountResult >= 0) {

			GameManager.Instance.coinsTotal = coinAmountResult;

		} else {

			PlayerPrefs.SetInt(coinAmountKey, 0);
			GameManager.Instance.coinsTotal = 0;

		}

	}

	// Update is called once per frame
	void Update() {

	}

	public void SetNewLongestDistance(int newDistance) {

		PlayerPrefs.SetInt(longestDistanceKey, newDistance);

	}

	public void SetCoinTotal(int newCoinsAmount) {

		PlayerPrefs.SetInt(coinAmountKey, newCoinsAmount);

	}

	/// <summary>
	/// Dangerous!!!
	/// </summary>
	public void ResetPrefs() {

		Debug.Log("Resetting PlayerPrefs...");

		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
		Start();

	}

	
}
