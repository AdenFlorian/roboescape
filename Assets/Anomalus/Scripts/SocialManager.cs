﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;

public class SocialManager : MonoBehaviour {

	public static SocialManager Instance;

	static string distanceLongestLeaderboard = "CgkI3Iqetr0NEAIQAQ";
	//static string distanceTotalLeaderboard = "CgkI3Iqetr0NEAIQCA";
	//string testIncAchievement = "< Incremental Achievement ID >";

	public static bool distancesSubmitted { get; private set; }

	// Use this for initialization
	void Start () {

		Instance = this;

		// Initialize properties here
		distancesSubmitted = false;

		Social.Active = new UnityEngine.SocialPlatforms.GPGSocial();

	}
	
	// Update is called once per frame
	void Update () {

		
	}

	public static void SignIn() {

		Social.localUser.Authenticate(OnAuthCB);

	}

	public static void SignOut() {

		NerdGPG.Instance().signOut();

		distancesSubmitted = false;

	}

	public static void SetDistancesSubmittedFalse() {

		distancesSubmitted = false;

	}

	
	private static void OnAuthCB(bool result) {

		if (result) {
			OnSignInSuccess();
		} else {
			string msg = "Logged out or Sign In failed";
			Debug.Log(msg);
			FlashMessagePanel.Instance.Add(msg, Color.red);
		}

	}

	private static void OnSignInSuccess() {

		string msg = "Sign in successful";
		Debug.Log(msg);
		FlashMessagePanel.Instance.Add(msg);
		//GameManager.Instance.SendMessage("OnSignedIn");

		SubmitDistances();

		AchievementsManager.Instance.LoadAchievements();

		// Get info from leaderboards
		//LoadLongestDistancesLeaderboard();


	}

	/// <summary>
	/// Submits users highest distance and total distance, one at a time; gets distances from GameManager
	/// </summary>
	public static void SubmitDistances() {

		Social.ReportScore(GameManager.Instance.distanceHighScore, distanceLongestLeaderboard, OnSubmitLongestDistance);
		
	}

	public static void OnSubmitLongestDistance(bool result) {

		if (result) {
			string msg = "Longest distance submitted successfully";
			Debug.Log(msg);
			FlashMessagePanel.Instance.Add(msg);
			//Social.ReportScore(GameManager.Instance.totalDistanceForUser, distanceTotalLeaderboard, OnSubmitTotalDistance);
		} else {
			string msg = "Longest distance submission failed!";
			Debug.Log(msg);
			FlashMessagePanel.Instance.Add(msg, Color.red);
		}

	}
	/*
	public static void OnSubmitTotalDistance(bool result) {

		if (result) {
			string msg = "Total distance submitted successfully";
			Debug.Log(msg);
			FlashMessagePanel.Instance.Add(msg);
		} else {
			string msg = "Total distance submission failed!";
			Debug.Log(msg);
			FlashMessagePanel.Instance.Add(msg, Color.red);
		}

		distancesSubmitted = true;

	}
	*/

	public static void ShowLongestDistanceLeaderboard() {

		//Social.ShowLeaderboardUI();	// Shows all leaderboards for game
		NerdGPG.Instance().showLeaderBoards(distanceLongestLeaderboard);	// Shows only designated leaderboard

	}

	/*
	static void LoadLongestDistancesLeaderboard() {

		Debug.Log("LoadingScores...");

		Social.LoadScores(distanceLongestLeaderboard, OnReceiveLongestDistanceScores);

	}

	public static void OnReceiveLongestDistanceScores(IScore[] scores) {

		if (scores.Length > 0) {

			Debug.Log("Got " + scores.Length + " scores");

			string myScores = "Leaderboard:\n";

			foreach (IScore score in scores)
				myScores += "\t" + score.userID + " " + score.formattedValue + " " + score.date + "\n";

			Debug.Log(myScores);

		} else {

			Debug.Log("No scores loaded");

		}

	}

	*/

}
