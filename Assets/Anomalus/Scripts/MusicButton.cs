﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class MusicButton : MonoBehaviour {

	public Texture speakerOn;
	public Texture speakerOff;

	public bool musicOn = true;
	/*
	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}
	*/
	void OnMouseUpAsButton() {

		GameManager.Instance.SwitchMusicOnOff();

		if (musicOn) {

			renderer.material.mainTexture = speakerOff;

		} else {

			renderer.material.mainTexture = speakerOn;

		}

		musicOn = !musicOn;

	}
	/*
	void OnMouseOver() {



	}

	void OnMouseEnter() {



	}

	void OnMouseExit() {



	}
	 */
}
