﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using System.Text;

public class AchievementsManager : MonoBehaviour {

	public static AchievementsManager Instance;

	IAchievement[] loadedAchievements;

	string achvmntID_FinishedARound =	"CgkI3Iqetr0NEAIQAg";
	string achvmntID_100Distance =		"CgkI3Iqetr0NEAIQBg";
	string achvmntID_250Distance =		"CgkI3Iqetr0NEAIQBw";
	string achvmntID_500Distance =		"CgkI3Iqetr0NEAIQBQ";
	string achvmntID_1000Distance =		"CgkI3Iqetr0NEAIQAw";
	string achvmntID_NoCoinsCollected =	"CgkI3Iqetr0NEAIQBA";

	// Use this for initialization
	void Start() {

		Instance = this;

	}

	// Update is called once per frame
	void Update() {

	}

	public void LoadAchievements() {

		Social.LoadAchievements(OnLoadAchievements);

	}

	public void OnLoadAchievements(IAchievement[] achievements) {

		if (achievements.Length > 0) {

			Debug.Log("Got " + achievements.Length + " achievement instances");

			StringBuilder myAchievements = new StringBuilder("My achievements:\n");

			foreach (IAchievement achievement in achievements) {
				myAchievements.Append("\t" +
					achievement.id + " " +
					achievement.percentCompleted + " " +
					achievement.completed + " " +
					achievement.lastReportedDate + "\n");
			}

			Debug.Log(myAchievements.ToString());

			Debug.Log("Loaded Achievements Successfully!");

			loadedAchievements = achievements;

		} else {

			Debug.Log("No achievements returned");

		}

	}

	#region GetAchievements

	public void GetAchievement_FinishedARound() {



	}

	public void GetAchievement_100Distance() {



	}

	public void GetAchievement_250Distance() {



	}

	public void GetAchievement_500Distance() {



	}

	public void GetAchievement_1000Distance() {



	}

	public void GetAchievement_NoCoinsCollected() {



	}

	#endregion 
}
