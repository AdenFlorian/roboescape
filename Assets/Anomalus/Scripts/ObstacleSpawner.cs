﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleSpawner : MonoBehaviour {

	public static ObstacleSpawner Instance;

	public Transform obstaclesParent;

	public GameObject newObstacle;

	public GameObject[] obstaclesToSpawn;
	public GameObject[] coinBatchesToSpawn;

	public GameObject obstacleSpawnPosition;

	bool coinSpawningOn = false;

	//SpawnPhase spawnPhase = SpawnPhase.Obstacles;

	public bool spawnObstacles = false;

	public float spawnRate = 1;	// Spawn per unit moved
	float spawnTimer = 0;

	// Use this for initialization
	void Start () {

		Random.seed = System.DateTime.Now.Millisecond;

		Instance = this;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (spawnObstacles) {
			spawnTimer += GameManager.Instance.scrollSpeed * Time.deltaTime;

			if (spawnTimer > spawnRate) {
				spawnTimer -= spawnRate;
				SpawnObastacle();
			}

		}
		
	
	}

	void SpawnObastacle() {

		if (spawnObstacles) {

			// Decide which obstacle to spawn
			GameObject nextObstacleGO = obstaclesToSpawn[Random.Range(0, obstaclesToSpawn.Length)];
			//Obstacle nextObstacle = nextObstacleGO.GetComponent<Obstacle>();

			// Holds instantiaed obstacle
			GameObject newObstacleGO;
			Obstacle newObstacle;

			// Instantiates from obstacle prefab
			newObstacleGO = Instantiate(nextObstacleGO, new Vector3(obstacleSpawnPosition.transform.position.x, Random.Range(-0.94f, 0.94f), 0), Quaternion.identity) as GameObject;

			newObstacle = newObstacleGO.GetComponent<Obstacle>();

			// Checks if obstacle is allowed to spawn in doubles, and spawns another if true
			if (newObstacle.canSpawnInDoubles && Random.Range(0,100) > 50) {
				(Instantiate(newObstacleGO, new Vector3(newObstacleGO.transform.position.x + .35f, -newObstacleGO.transform.position.y * 0.75f, 0), Quaternion.identity) as GameObject).transform.parent = obstaclesParent;
			}

			if (newObstacle.canBeRotatedOnSpawn) {
				newObstacleGO.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 359.9f));
			}
			
			// Puts all obstacles as children of obstaclesParent
			newObstacleGO.transform.parent = obstaclesParent;
		}

		//Debug.Log("Obstacle spawned");
		
	}

	public void RemoveAllObstacles() {

		/*
		Transform[] obstaclesToRemove = obstaclesParent.GetComponentsInChildren<Transform>();

		for (int i = 0; i < obstaclesToRemove.Length; i++) {
			Destroy(obstaclesToRemove[i].gameObject);
		}
		*/

		Destroy(obstaclesParent.gameObject);

		obstaclesParent = (Instantiate(new GameObject("ObstaclesParent")) as GameObject).transform;

	}

	public void StartSpawning() {

		Debug.Log("Start Spawning!");

		if (coinSpawningOn) {

			StartCoroutine(StartCoinPhaseLoop(10));
			
		}

		spawnObstacles = true;

	}

	public void StopSpawning() {

		Debug.Log("Stop Spawning!");

		StopAllCoroutines();

		spawnObstacles = false;
		spawnTimer = 0;

	}



	IEnumerator StartCoinPhaseLoop(int timeBeforeEachPhase) {

		while (true) {

			yield return new WaitForSeconds(timeBeforeEachPhase);

			spawnObstacles = false;

			yield return new WaitForSeconds(2);

			GameObject newCoinBatchGO = coinBatchesToSpawn[Random.Range(0, coinBatchesToSpawn.Length)];

			// Spawn coin batch
			newCoinBatchGO = Instantiate(newCoinBatchGO, new Vector3(obstacleSpawnPosition.transform.position.x, 0, 0), Quaternion.identity) as GameObject;

			newCoinBatchGO.transform.parent = obstaclesParent;

			yield return new WaitForSeconds(3);

			spawnObstacles = true;
			
		}

		

	}

	public enum SpawnPhase {
		Obstacles,
		Coins
	}
}
