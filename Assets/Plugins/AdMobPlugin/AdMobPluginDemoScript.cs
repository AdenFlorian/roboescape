using UnityEngine;

// Example script showing how you can easily call into the AdMobPlugin.
public class AdMobPluginDemoScript : MonoBehaviour {

    void Start()
    {
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		print("Started");
        AdMobPlugin.CreateBannerView("INSERT_YOUR_AD_UNIT_ID_HERE",
                                     AdMobPlugin.AdSize.Banner,
                                     true);
        print("Created Banner View");
        AdMobPlugin.RequestBannerAd(true);
        print("Requested Banner Ad");
#endif
    }

    void OnEnable()
    {
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		print("Registering for AdMob Events");
        AdMobPlugin.ReceivedAd += HandleReceivedAd;
        AdMobPlugin.FailedToReceiveAd += HandleFailedToReceiveAd;
        AdMobPlugin.ShowingOverlay += HandleShowingOverlay;
        AdMobPlugin.DismissedOverlay += HandleDismissedOverlay;
        AdMobPlugin.LeavingApplication += HandleLeavingApplication;
#endif
    }

    void OnDisable()
    {
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
        print("Unregistering for AdMob Events");
		AdMobPlugin.ReceivedAd -= HandleReceivedAd;
        AdMobPlugin.FailedToReceiveAd -= HandleFailedToReceiveAd;
        AdMobPlugin.ShowingOverlay -= HandleShowingOverlay;
        AdMobPlugin.DismissedOverlay -= HandleDismissedOverlay;
        AdMobPlugin.LeavingApplication -= HandleLeavingApplication;
#endif
    }

    public void HandleReceivedAd()
    {
        print("HandleReceivedAd event received");
    }

    public void HandleFailedToReceiveAd(string message)
    {
        print("HandleFailedToReceiveAd event received with message:");
        print(message);
    }

    public void HandleShowingOverlay()
    {
        print("HandleShowingOverlay event received");
    }

    public void HandleDismissedOverlay()
    {
        print("HandleDismissedOverlay event received");
    }

    public void HandleLeavingApplication()
    {
        print("HandleLeavingApplication event received");
    }
}